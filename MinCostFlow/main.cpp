#include <iostream>
#include "deploy.h"
#include "lib_io.h"
#include "lib_time.h"
#include "stdio.h"

using namespace std;

int main()
{
    print_time("Begin");
    char *topo[MAX_EDGE_NUM];
    int line_num;

    //char *topo_file = "F:\\MinCostFlow\\MinCostFlow\\case_example\\now_0401\\2\\case5.txt";

    char *topo_file = "F:\\MinCostFlow\\MinCostFlow\\case_example\\case0.txt";

    line_num = read_file(topo, MAX_EDGE_NUM, topo_file);

    printf("line num is :%d \n", line_num);
    if (line_num == 0)
    {
        printf("Please input valid topo file.\n");
        return -1;
    }

    char *result_file = "F:\\MinCostFlow_flow\\MinCostFlow\\result.txt";

    deploy_server(topo, line_num, result_file);

    release_buff(topo, line_num);

    print_time("End");

    return 0;
}
