﻿#include "deploy.h"
#include <stdio.h>
#include "GA.h"
#include "SA.h"
#include "flow.h"
#include "graph.h"
#include <iostream>
#include <cstring>
#include<iomanip>
#include <time.h>
#include <fstream>
#include <math.h>

/**<
timer on linux platform, and can not run on window platform
Ref: http://www.cnblogs.com/dandingyy/articles/2653218.html
 */
/*
#include <unistd.h>        //pause()
#include <signal.h>        //signal()
#include <sys/time.h>    //struct itimerval, setitimer()
#include <iostream>

void TimerOutFunc(int signo);
void startTimer()
{
    struct itimerval tick;

    signal(SIGALRM, TimerOutFunc);
    memset(&tick, 0, sizeof(tick));

    //Timeout to run first time
    tick.it_value.tv_sec = 88;
    tick.it_value.tv_usec = 0;

    //After first, the Interval time for clock
    tick.it_interval.tv_sec = 88;
    tick.it_interval.tv_usec = 0;

    if(setitimer(ITIMER_REAL, &tick, NULL) < 0)
            printf("Set timer failed!\n");

}
*/

LinkGraph* graph = NULL;
MinCostFlow* flow = NULL;
GeneAlg* ga = NULL;
SimulatedAnnealing *sa = NULL;
char* saveFile = NULL;
void deletePtr();

clock_t start, finish;
void startTime();
void finishTime();
void printTime();
void flowTest(LinkGraph* graph, char * filename);
void testParamsForSA(LinkGraph* graph, MinCostFlow* flow, char* filename);
void testParamsForGA(LinkGraph* graph, MinCostFlow* flow, char* filename);

//你要完成的功能总入口
void deploy_server(char * topo[MAX_EDGE_NUM], int line_num,char * filename)
{
    //timer on linux platform, turn on the code before uploading
//    startTimer();
    //create graph structure
    graph = new LinkGraph();
    saveFile = filename;

    //startTime();

    graphCreating(graph, topo, line_num);
    flow = new MinCostFlow(graph);

    //flowTest(graph, filename);
    srand((unsigned)time(NULL));

    SimulatedAnnealingEntrance(graph, flow, filename);
  //  testParamsForSA(graph, flow, filename);

    //geneticAlgorithmEntrance(graph, flow, filename);
    //testParamsForGA(graph,flow,filename);

    //finishTime();
    //printTime();
    deletePtr();
}

void TimerOutFunc(int signo)
{
    //SA
    if(sa != NULL)
    {
        sa->SetSwitch(true);
    }

    //GA
    if(ga != NULL)
    {
        ga->writeFile(saveFile);
    }
}

void deletePtr()
{
    if(graph != NULL)
    {
        delete graph;
        graph = NULL;
    }

    if(flow != NULL)
    {
        delete flow;
        flow = NULL;
    }

    if(ga != NULL)
    {
        delete ga;
        ga = NULL;
    }

    if(sa != NULL)
    {
        delete sa;
        sa = NULL;
    }
}
void geneticAlgorithmEntrance(LinkGraph* graph, MinCostFlow* flow, char* filename)
{
    ga = new GeneAlg(graph, flow,20,50,0.8,0.035,filename);
}

void testParamsForGA(LinkGraph* graph, MinCostFlow* flow, char* filename)
{
    int popsize;
    double psrate;
    double pmrate;
    int maxgene;
    int BestFitness=0;

    std::ofstream outfile("F:\\MinCostFlow\\MinCostFlow\\case_example\\GA_parament_test.txt", std::ios::out);
    for(psrate=0.00; psrate<0.8; psrate+=0.05)
    {
        for(pmrate=0.000; pmrate<0.1; pmrate+=0.005)
        {
            for(maxgene=50; maxgene<51; maxgene+=50)
            {
                for(popsize=200; popsize<201; popsize+=50)
                {
                    ga = new GeneAlg(graph,flow,popsize,maxgene,psrate,pmrate,filename);
                    outfile<<psrate<<",";
                    outfile<<pmrate<<",";
                    outfile<<maxgene<<",";
                    outfile<<ga->bestChromosome.fitness<<std::endl;
                    if(BestFitness<ga->bestChromosome.fitness)
                        BestFitness=ga->bestChromosome.fitness;
                }
            }
        }
    }
    outfile.close();
    std::cout<<"BestFitness:"<<BestFitness<<std::endl;
}

void SimulatedAnnealingEntrance(LinkGraph* graph, MinCostFlow* flow, char* filename)
{
    SAFitness safit;

    if(graph->commonNodeNum < 251)
    {
        safit.deltaTemp = 0.98;
        safit.finalTemp = 1e-8;
        safit.initTemp = 1500;
        safit.inLoop = 40;

        safit.outLoop = 30;
        safit.upperLimit = 500;

        safit.sum = 100;
        safit.addNum = 50;
        safit.WeightedNum = 2;

        safit.isTaboo = true;
        safit.tabooNum = 3;
    }
    else if(graph->commonNodeNum < 501)
    {
        safit.deltaTemp = 0.98;
        safit.finalTemp = 1e-8;
        safit.initTemp = 200;
        safit.inLoop = 42;

        safit.outLoop = 20;
        safit.upperLimit = 190;

        safit.sum = 100;
        safit.addNum = 55;
        safit.WeightedNum = 2;

        safit.isTaboo = true;
        safit.tabooNum = 3;
    }
    else
    {
        safit.deltaTemp = 0.98;
        safit.finalTemp = 1e-8;
        safit.initTemp = 75;
        safit.inLoop = 5;

        safit.outLoop = 5;
        safit.upperLimit = 20;

        safit.sum = 100;
        safit.addNum = 50;
        safit.WeightedNum = 0;

        safit.isTaboo = true;//false
        safit.tabooNum = 4;
    }

    sa = new SimulatedAnnealing(graph, flow, safit);
    sa->GetServerGroup(filename);
}

void testParamsForSA(LinkGraph* graph, MinCostFlow* flow, char* filename)
{
    SAFitness safit;
    safit.deltaTemp = 0.98;
    safit.finalTemp = 1e-8;
//    safit.initTemp = 3000;
//    safit.outLoop = 350;
//    safit.inLoop = 50;
//    safit.upperLimit = 500;
    safit.sum = 100;
//    safit.addNum = 5;
//    safit.WeightedNum = 4;

    std::ofstream outfile("F:\\MinCostFlow\\MinCostFlow\\case_example\\new\\0\\case0_test3.txt", std::ios::out);

    SimulatedAnnealing *sa_tmp;

for(safit.addNum = 25; safit.addNum <= 40; safit.addNum += 5)
{
    for(safit.WeightedNum = 5; safit.WeightedNum <= safit.addNum; safit.WeightedNum += 5)
    {
        for(safit.initTemp = 10; safit.initTemp <= 510; safit.initTemp += 100)
        {
//            double tempnum = log(safit.finalTemp/(double)safit.initTemp)/log(safit.deltaTemp);
            for(safit.outLoop = 10; safit.outLoop <= 20; safit.outLoop += 2)
            {
                for(safit.inLoop = 10; safit.inLoop <= 20; safit.inLoop += 2)
                {
                    for(safit.upperLimit = 10; safit.upperLimit <= safit.inLoop; safit.upperLimit += 2)
                    {

                        sa_tmp = new SimulatedAnnealing(graph, flow, safit);

                        start = clock();
                        std::cout<<"--------------------------------"<<std::endl;
                        sa_tmp->Simulation();

                        finish = clock();

                        outfile<<sa_tmp->bestGroup.cost<<",";
                        outfile<<sa_tmp->topGroup.cost<<",";
                        outfile<<(double)(finish - start) / CLOCKS_PER_SEC<<",";
                        outfile<<safit.deltaTemp<<",";
                        outfile<<safit.finalTemp<<",";
                        outfile<<safit.initTemp<<",";
                        outfile<<safit.outLoop<<",";
                        outfile<<safit.inLoop<<",";
                        outfile<<safit.upperLimit<<",";
                        outfile<<safit.sum<<",";
                        outfile<<safit.addNum<<",";
                        outfile<<safit.WeightedNum<<std::endl;


                        delete sa_tmp;
                        sa_tmp = NULL;
                    }
                }
            }
        }
    }
}

    outfile.close();
}

void flowTest(LinkGraph* graph, char * filename)
{
    MinCostFlow minCostFlow(graph);

    std::vector<int> serverVec;
    serverVec.push_back(299);
    serverVec.push_back(100);
    serverVec.push_back(725);
    serverVec.push_back(539);
    serverVec.push_back(351);
    serverVec.push_back(654);
    serverVec.push_back(79);
    serverVec.push_back(631);
    serverVec.push_back(52);
    serverVec.push_back(474);
    serverVec.push_back(702);
    serverVec.push_back(56);
    serverVec.push_back(253);
    serverVec.push_back(95);
    serverVec.push_back(645);
    serverVec.push_back(81);
    serverVec.push_back(176);
    serverVec.push_back(365);
    serverVec.push_back(84);
    serverVec.push_back(188);
    serverVec.push_back(224);
    serverVec.push_back(78);
    serverVec.push_back(105);
    serverVec.push_back(271);
    serverVec.push_back(119);
    serverVec.push_back(577);

    serverVec.push_back(355);

    serverVec.push_back(509);

    serverVec.push_back(263);

    serverVec.push_back(658);

    serverVec.push_back(534);

    serverVec.push_back(416);

    serverVec.push_back(677);

    serverVec.push_back(288);

    serverVec.push_back(358);

    serverVec.push_back(620);

    serverVec.push_back(580);

    serverVec.push_back(532);

    serverVec.push_back(433);

    serverVec.push_back(653);

    serverVec.push_back(454);

    serverVec.push_back(516);


    serverVec.push_back(439);

    serverVec.push_back(144);

    serverVec.push_back(714);

    serverVec.push_back(311);

    serverVec.push_back(624);

    serverVec.push_back(779);

    serverVec.push_back(163);

    serverVec.push_back(109);

    serverVec.push_back(180);

    serverVec.push_back(466);

    serverVec.push_back(502);

    serverVec.push_back(732);

    serverVec.push_back(217);

    serverVec.push_back(579);

    serverVec.push_back(568);

    serverVec.push_back(798);

    serverVec.push_back(239);

    serverVec.push_back(589);

    serverVec.push_back(379);

    serverVec.push_back(760);


    serverVec.push_back(324);

    serverVec.push_back(486);

    serverVec.push_back(143);

    serverVec.push_back(54);

    serverVec.push_back(387);

    serverVec.push_back(65);

    serverVec.push_back(767);

    serverVec.push_back(289);

    serverVec.push_back(93);

    serverVec.push_back(248);

    serverVec.push_back(556);

    serverVec.push_back(131);

    serverVec.push_back(744);

    serverVec.push_back(31);

    serverVec.push_back(699);

    serverVec.push_back(138);

    serverVec.push_back(50);

    serverVec.push_back(772);

    serverVec.push_back(665);

    serverVec.push_back(434);

    serverVec.push_back(713);


    serverVec.push_back(211);

    serverVec.push_back(614);

    serverVec.push_back(728);

    serverVec.push_back(187);

    serverVec.push_back(276);

    serverVec.push_back(537);

    serverVec.push_back(53);

    serverVec.push_back(531);

    serverVec.push_back(664);

    serverVec.push_back(550);

    serverVec.push_back(280);

    serverVec.push_back(464);

    serverVec.push_back(789);

    serverVec.push_back(407);

    serverVec.push_back(75);

    serverVec.push_back(68);

    serverVec.push_back(265);

    serverVec.push_back(330);

    serverVec.push_back(213);

    serverVec.push_back(536);

    serverVec.push_back(166);


    serverVec.push_back(468);

    serverVec.push_back(477);

    serverVec.push_back(610);

    serverVec.push_back(132);

    serverVec.push_back(547);

    serverVec.push_back(141);

    serverVec.push_back(600);

    serverVec.push_back(232);

    serverVec.push_back(270);

    serverVec.push_back(383);

    serverVec.push_back(62);

    serverVec.push_back(378);

    serverVec.push_back(209);

    serverVec.push_back(458);

    serverVec.push_back(506);

    serverVec.push_back(364);

    serverVec.push_back(194);

    serverVec.push_back(48);

    serverVec.push_back(220);

    serverVec.push_back(332);

    serverVec.push_back(266);



    serverVec.push_back(716);

    serverVec.push_back(730);

    serverVec.push_back(567);

    serverVec.push_back(758);

    serverVec.push_back(523);

    serverVec.push_back(479);

    serverVec.push_back(501);

    serverVec.push_back(787);

    serverVec.push_back(256);

    serverVec.push_back(750);

    serverVec.push_back(576);

    serverVec.push_back(17);

    serverVec.push_back(419);

    serverVec.push_back(251);

    serverVec.push_back(323);

    serverVec.push_back(715);

    graph->reconstructSuperSource(serverVec, serverVec.size());

    start = clock_t();
    //int cost = minCostFlow.getCost();
    int cost = minCostFlow.getFinalCost();
    if(cost == INFI)
    {
        std::cout<<"INFI"<<std::endl;
    }
    else
    {
        std::cout<<"cost: "<<cost<<std::endl;
    }

    minCostFlow.saveResultToFile(filename);

}

void graphCreating(LinkGraph* graph, char * topo[MAX_EDGE_NUM], int line_num)
{
//    int enterNum = 0;
    char* line = NULL;
    char* p0 = NULL;
    char* p1 = NULL;
    char* p2 = NULL;
    char* p3 = NULL;
    char space[] = " ";
    int commonNodeNum = 0;
    int commonNodeEdgeNum = 0;
    int costNodeNum = 0;

    line = topo[0];
    p0 = strtok(line,space);
    p1 = strtok(NULL,space);
    p2 = strtok(NULL,space);
    commonNodeNum = std::atoi(p0);
    commonNodeEdgeNum = std::atoi(p1);
    costNodeNum = std::atoi(p2);
    graph->init(commonNodeNum, commonNodeEdgeNum, costNodeNum);

    line = topo[2];
    p0 = strtok(line,space);
    graph->eachServerCost = std::atoi(p0);

    for(int i = 0; i < commonNodeEdgeNum; i++)
    {
        line = topo[i + 4];

        p0 = strtok(line,space);
        p1 = strtok(NULL,space);
        p2 = strtok(NULL,space);
        p3 = strtok(NULL,space);

        graph->addEdge(std::atoi(p0), std::atoi(p1), std::atoi(p2), std::atoi(p3));

        //graph->edgeVec[graph->curEdgeNum - 2].print();
        //graph->edgeVec[graph->curEdgeNum - 1].print();

        graph->addEdge(std::atoi(p1), std::atoi(p0), std::atoi(p2), std::atoi(p3));
        //graph->edgeVec[graph->curEdgeNum - 2].print();
        //graph->edgeVec[graph->curEdgeNum - 1].print();
    }

    for(int i = 0; i < costNodeNum; i++)
    {
        line = topo[i + 5 + commonNodeEdgeNum];

        p0 = strtok(line,space);
        p1 = strtok(NULL,space);
        p2 = strtok(NULL,space);

        graph->addEdge(std::atoi(p1), std::atoi(p0) + graph->commonNodeNum, std::atoi(p2), 0);

        //graph->edgeVec[graph->curEdgeNum - 2].print();
        //graph->edgeVec[graph->curEdgeNum - 1].print();

        /**< test code */
        graph->costNodePairs.push_back(std::pair<int,int>(std::atoi(p1), std::atoi(p0) + graph->commonNodeNum));
        graph->boundNodes.push_back(std::atoi(p1));
        graph->costNodes.push_back(std::atoi(p0) + graph->commonNodeNum);
        graph->costDemands.push_back(std::atoi(p2));

        graph->flowSum += std::atoi(p2);
    }

    graph->updateNodeProperty();

/**< find the mandatory Nodes and impossible nodes */
    if(graph->commonNodeNum < 251)
    {
//        graph->preProcessing();
    }
    else if(graph->commonNodeNum < 501)
    {
//        graph->preProcessing();
    }
    else
    {
        graph->preProcessing();
    }

    //add super target
    for(size_t i = 0; i < graph->costNodes.size(); i++)
    {
        graph->addEdge(graph->costNodes[i], graph->targetIdx, graph->costDemands[i], 0);
//        graph->edgeVec[graph->curEdgeNum - 2].print();
//        graph->edgeVec[graph->curEdgeNum - 1].print();
    }

    graph->sourceEdgeStartIdx = graph->curEdgeNum;
}

void startTime()
{
    start = clock();
}
void finishTime()
{
    finish = clock();
}
void printTime()
{
    std::cout<<"Time: "<<(double)(finish - start) / CLOCKS_PER_SEC<<std::endl;
}
