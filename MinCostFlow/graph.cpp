#include <iostream>
#include "graph.h"

void Edge::print()
{
    std::cout<<std::endl<<"idx: "<<idx<<std::endl;
    std::cout<<from<<" -> "<<to<<" next: "<<next<<std::endl;
    std::cout<<"updatedCapacity: "<<updatedCapacity<<" ("<<originCapacity<<") "<<std::endl;
    std::cout<<"cost: "<<cost<<" bw: "<<bw<<std::endl;
}

double Node::calculateWeight(double weightOfDegree, double weightOfBandWidth, double weightOfInnerNode)
{
    weight = weightOfDegree * degreeRatio
    + weightOfBandWidth * bandWidthRatio
    + weightOfInnerNode * isInner;

    return weight;
}

LinkGraph::~LinkGraph()
{
    sortedCommonNodes.clear();
    costNodePairs.clear();
    sources.clear();
    costDemands.clear();
    edgeVec.clear();
    nodeVec.clear();
}

void LinkGraph::init(int _commonNodeNum, int _commonNodeEdgeNum, int _costNodeNum)
{
    commonNodeNum = _commonNodeNum;
    commonNodeEdgeNum = _commonNodeEdgeNum;
    costNodeNum = _costNodeNum;

    //common edge + target edge + source edge
    edgeVec.resize(commonNodeEdgeNum * 4 + costNodeNum * (2 + 2) + commonNodeNum * 2);
    nodeVec.resize(commonNodeNum + costNodeNum + 2);
    nodeTypes.resize(commonNodeNum + costNodeNum + 2);

    curEdgeNum = 0;

    sourceIdx = nodeVec.size() - 2;
    targetIdx = nodeVec.size() - 1;

    sourceEdgeStartIdx = commonNodeEdgeNum * 4 + costNodeNum * (2 + 2);

    flowSum = 0;

    sortedCommonNodes.resize(commonNodeNum);
    for(int i =0 ; i < commonNodeNum; i++)
    {
        sortedCommonNodes[i] = i;
    }

    //preprocessing
    impossibleNodesSelected.resize(commonNodeNum);
    mandatoryNodesSelected.resize(commonNodeNum);
    passNodesSelected.resize(commonNodeNum);
}

void LinkGraph::addEdge(int _from, int _to, int _capacity, int _cost)
{
    edgeVec[curEdgeNum].idx = curEdgeNum;
    edgeVec[curEdgeNum].from = _from;
    edgeVec[curEdgeNum].to = _to;
    edgeVec[curEdgeNum].cost = _cost;
    edgeVec[curEdgeNum].next = nodeVec[_from].edgeHead;
    edgeVec[curEdgeNum].updatedCapacity = _capacity;
    edgeVec[curEdgeNum].originCapacity = _capacity;
    edgeVec[curEdgeNum].bw = _capacity;
    nodeVec[_from].edgeHead = curEdgeNum++;

    edgeVec[curEdgeNum].idx = curEdgeNum;
    edgeVec[curEdgeNum].from = _to;
    edgeVec[curEdgeNum].to = _from;
    edgeVec[curEdgeNum].cost = -_cost;
    edgeVec[curEdgeNum].next = nodeVec[_to].edgeHead;
    edgeVec[curEdgeNum].updatedCapacity = 0;
    edgeVec[curEdgeNum].originCapacity = 0;
    edgeVec[curEdgeNum].bw = _capacity;
    nodeVec[_to].edgeHead = curEdgeNum++;
}

void LinkGraph::reconstructSuperSource(std::vector<int>& _serverVec, int _length)
{
    if(0 == _length) return;

    //delete s->ni and ni->s half edges
    for(int ei = nodeVec[sourceIdx].edgeHead; ei != -1; ei = edgeVec[ei].next)
    {
        nodeVec[edgeVec[ei].to].edgeHead = edgeVec[ei + 1].next;
    }

    nodeVec[sourceIdx].edgeHead = -1;
    curEdgeNum = sourceEdgeStartIdx;

    /**< test code */
    sources.clear();

    //reconstruct s->ni and ni->s
    for(int i = 0; i < _length; i++)
    {
        addEdge(sourceIdx, _serverVec[i], flowSum, 0);

        /**< test code */
        sources.push_back(_serverVec[i]);
    }
}

void LinkGraph::restoreEdgeCapacity()
{
    for(int i = 0; i < curEdgeNum; i++)
    {
        edgeVec[i].updatedCapacity = edgeVec[i].originCapacity;
    }
}

void LinkGraph::sortCostNodeAccordingDemand(double weightOfDegree, double weightOfBandWidth, double weightOfInnerNode)
{
    for(int i = 0; i < commonNodeNum; i ++)
    {
        nodeVec[i].calculateWeight(weightOfDegree, weightOfBandWidth, weightOfInnerNode);
    }

    double* weightVec = new double[commonNodeNum];
    for(int i = 0; i < commonNodeNum; i++)
    {
        weightVec[i] = nodeVec[sortedCommonNodes[i]].weight;
    }

//    for(int i = 0; i < commonNodeNum; i++)
//    {
//        std::cout<<weightVec[i]<<" ";
//    }
//    std::cout<<std::endl;
//    for(int i = 0; i < commonNodeNum; i++)
//    {
//        std::cout<<sortedCommonNodes[i]<<" ";
//    }

    quickSort(weightVec, 0 , commonNodeNum - 1);

//    std::cout<<std::endl<<std::endl;
//    for(int i = 0; i < commonNodeNum; i++)
//    {
//        std::cout<<weightVec[i]<<" ";
//    }
//    std::cout<<std::endl;
//    for(int i = 0; i < commonNodeNum; i++)
//    {
//        std::cout<<sortedCommonNodes[i]<<" ";
//    }
//    std::cout<<std::endl;

    delete weightVec;
    weightVec = NULL;
}

void LinkGraph::quickSort(double* a,int left,int right)
{
    if(left < right)
    {
        double x = a[right];

        int i = left-1, j = right, p = left-1, q = right;
        for (;;)
        {
            while (a[++i] < x)
            {
            }
            while (a[--j] > x)
            {
                if(j==left)
                {
                    break;
                }
            }
            if(i < j)
            {
                std::swap(a[i], a[j]);
                std::swap(sortedCommonNodes[i], sortedCommonNodes[j]);

                if (a[i] == x)
                {
                    p++;
                    std::swap(a[p], a[i]);
                    std::swap(sortedCommonNodes[p], sortedCommonNodes[i]);
                }
                if (a[j] == x)
                {
                    q--;
                    std::swap(a[q], a[j]);
                    std::swap(sortedCommonNodes[q], sortedCommonNodes[j]);
                }
            }
            else
            {
                break;
            }
        }
        std::swap(a[i], a[right]);
        std::swap(sortedCommonNodes[i], sortedCommonNodes[right]);

        j = i-1;
        i=i+1;
        for (int k=left; k<=p; k++, j--)
        {
            std::swap(a[k], a[j]);
            std::swap(sortedCommonNodes[k], sortedCommonNodes[j]);
        }
        for (int k=right-1; k>=q; k--, i++)
        {
            std::swap(a[i], a[k]);
            std::swap(sortedCommonNodes[i], sortedCommonNodes[k]);
        }

        quickSort(a, left, j);
        quickSort(a, i, right);
    }
}

void LinkGraph::initNodeTypes()
{
    for(size_t i = 0; i < boundNodes.size(); i++)
    {
        nodeTypes[ boundNodes[i] ] = 1;//bound node
    }

    for(int i = commonNodeNum; i < commonNodeNum + costNodeNum; i++)
    {
        nodeTypes[i] = 2;//cost node
    }

    nodeTypes[commonNodeNum + costNodeNum] = 3;//source
    nodeTypes[commonNodeNum + costNodeNum + 1] = 4;//target

//    for(int i = 0; i < nodeTypes.size(); i++)
//    {
//        std::cout<<"node: "<<i<<" : "<<nodeTypes[i]<<std::endl;
//    }
}

void LinkGraph::updateAdjacentsForEachNode()
{
    for(int i = 0; i < commonNodeNum + costNodeNum; i++)
    {
        for(int ei = nodeVec[i].edgeHead; ei != -1; ei = edgeVec[ei].next)
        {
            if(nodeVec[i].adjacentNodes.size() == 0
               || nodeVec[i].adjacentNodes.back() != edgeVec[ei].to)
            {
                nodeVec[i].adjacentNodes.push_back(edgeVec[ei].to);
                nodeVec[i].adjacentEdges.push_back(ei);
            }
        }

//        std::cout<<"node "<<i<<" : ";
//        for(int j = 0; j < nodeVec[i].adjacentNodes.size(); j++)
//        {
//            std::cout<<nodeVec[i].adjacentNodes[j]<<"(to: "<<edgeVec[nodeVec[i].adjacentEdges[j]].to<<" ) ";
//        }
//        std::cout<<std::endl;
    }
}

void LinkGraph::updateNodeProperty()
{
    initNodeTypes();
    updateAdjacentsForEachNode();

    int maxDegree = -1;
    int maxBandWidth = -1;

    for(int i = 0; i < commonNodeNum; i ++)
    {
        nodeVec[i].degree = nodeVec[i].adjacentNodes.size();
        for(size_t j = 0; j < nodeVec[i].adjacentEdges.size(); j++)
        {
            nodeVec[i].bandWidth += edgeVec[ nodeVec[i].adjacentEdges[j] ].bw;
        }
        nodeVec[i].inputCapacity = nodeVec[i].bandWidth;

        if(1 == nodeTypes[i])//bound node
        {
            nodeVec[i].isInner = 0.0;
            nodeVec[i].inputCapacity -= edgeVec[ nodeVec[i].adjacentEdges[0] ].bw;

//            std::cout<<std::endl;
//            std::cout<<"costNodePairs[i].first: "<<i<<" "
//            <<nodeVec[i].inputCapacity << " "
//            <<nodeVec[i].bandWidth;
//
//            std::cout<<std::endl;
//            for(int ei = nodeVec[i].edgeHead; ei != -1; ei = edgeVec[ei].next)
//            {
//                std::cout<<"to "<< edgeVec[ei].to<<" ("<<edgeVec[ei].originCapacity<<")"<<std::endl;
//            }
        }

        if(nodeVec[i].degree > maxDegree)
        {
            maxDegree = nodeVec[i].degree;
        }

        if(nodeVec[i].bandWidth > maxBandWidth)
        {
            maxBandWidth = nodeVec[i].bandWidth;
        }
        //std::cout<<i<<" degree: "<<nodeVec[i].degree<<" bandWidth: "<<nodeVec[i].bandWidth<<std::endl;
    }

    for(int i = 0; i < commonNodeNum; i ++)
    {
        nodeVec[i].degreeRatio = nodeVec[i].degree * 1.0 / maxDegree;
        nodeVec[i].bandWidthRatio = nodeVec[i].bandWidth * 1.0 / maxBandWidth;

        //std::cout<<i<<" degreeRatio: "<<nodeVec[i].degreeRatio<<" bandWidthRatio: "<<nodeVec[i].bandWidthRatio<<std::endl;
    }
}

void LinkGraph::printKeyGraphInfo()
{
    std::cout<<std::endl;
    std::cout<<"commonNodeNum: "<<commonNodeNum<<std::endl;
    std::cout<<"commonNodeEdgeNum: "<<commonNodeEdgeNum<<std::endl;
    std::cout<<"costNodeNum: "<<costNodeNum<<std::endl;

    std::cout<<"nodeVec.size(): "<<nodeVec.size()<<std::endl;
    std::cout<<"sourceIdx: "<<sourceIdx<<std::endl;
    std::cout<<"targetIdx: "<<targetIdx<<std::endl;

    std::cout<<"edgeVec.size(): "<<edgeVec.size()<<std::endl;
    std::cout<<"curEdgeNum: "<<curEdgeNum<<std::endl;
    std::cout<<"sourceEdgeStartIdx: "<<sourceEdgeStartIdx<<std::endl;

    std::cout<<"flowSum: "<<flowSum<<std::endl;

    std::cout << std::endl;
    std::cout << "sources:" << std::endl;
    for( size_t si = 0; si < sources.size(); si++ )
    {
        std::cout << sourceIdx << " -> " << sources[si] << std::endl;
    }
    std::cout << std::endl;

    std::cout << "pairs:" << std::endl;
    for( size_t ti = 0; ti < costNodePairs.size(); ti++ )
    {
        std::cout << costNodePairs[ti].first << " -> "
        << costNodePairs[ti].second << " -> "
        << targetIdx << std::endl;
    }
    std::cout << std::endl;
}

void LinkGraph::preProcessing()
{
    //2. c class: branch cutting
    branchCutting();

//    std::cout<<"impossibleNodes: ";
//    for(size_t i = 0; i < impossibleNodes.size(); i++)
//    {
//        std::cout<<impossibleNodes[i]<<"("<<impossibleNodesSelected[impossibleNodes[i]]<<") ";
//    }
//    std::cout<<std::endl;
//
//    std::cout<<std::endl<<"findMandatoryNodes"<<std::endl;
    //3. a class : find mandatory Nodes
    findMandatoryNodes();

//    std::cout<<"mandatoryNodes: ";
//    for(size_t i = 0; i < mandatoryNodes.size(); i++)
//    {
//        std::cout<<mandatoryNodes[i]<<"("<<mandatoryNodesSelected[mandatoryNodes[i]]<<") ";
//    }
//    std::cout<<std::endl;
//
//    std::cout<<"impossibleNodes: ";
//    for(size_t i = 0; i < impossibleNodes.size(); i++)
//    {
//        std::cout<<impossibleNodes[i]<<"("<<impossibleNodesSelected[impossibleNodes[i]]<<") ";
//    }
//    std::cout<<std::endl;
}

void LinkGraph::findMandatoryNodes()
{
    std::vector<int> path;
    std::vector<int> costNodeVisit;
    costNodeVisit.resize(costNodes.size());

    for(int i = 0; i < (int)costNodes.size(); i++)
    {
        if(costNodeVisit[i])
        {
            continue;
        }

        findUnipath(costNodes[i], path, false, true);//set visit, cross bound node
        cancelEdgeVisitState(path);

//        for(size_t j = 0; j < path.size(); j++)
//        {
//            std::cout<<edgeVec[ path[j]].from<<" -> ";
//        }
//        std::cout<<"("<<edgeVec[path.back()].to<<")"<<std::endl;

        if(1 == path.size())//no long branch
        {
            getFromShortPath(i, path);
        }
        else//long branch
        {
            if(nodeTypes[ edgeVec[path.back()].to ] != 2)
            {
                getFromLongPath(i, path);
            }
            else//cost node
            {
                getFromLongPathBetweenCostNode(i, path);

                for(size_t j = 0; j < costNodes.size(); j++)
                {
                    if(costNodes[j] == edgeVec[path.back()].to)
                    {
                        costNodeVisit[j] = 1;
                    }
                }

            }
        }

        costNodeVisit[i] = 1;
        path.clear();
        //break;
    }
}

void LinkGraph::getFromShortPath(int& costNodeIdx, std::vector<int>& _path)
{
//    std::cout<<"getFromShortPath"<<std::endl;

    int inputBW = 0;
    int adjEdge = 0;
    for(size_t j = 1; j < nodeVec[ boundNodes[costNodeIdx] ].adjacentEdges.size(); j++)
    {
        adjEdge = nodeVec[ boundNodes[costNodeIdx] ].adjacentEdges[j];
        if( !edgeVec[adjEdge].isCutted )
        {
            inputBW += edgeVec[adjEdge].bw;
        }
    }

    adjEdge = nodeVec[ boundNodes[costNodeIdx] ].adjacentEdges[0];
    if(edgeVec[adjEdge].bw > inputBW)
    {
//        std::cout<<"edgeVec[adjEdge].bw: "<<edgeVec[adjEdge].bw<<" inputBW: "<<inputBW<<std::endl;

        mandatoryNodes.push_back( boundNodes[costNodeIdx] );
        mandatoryNodesSelected[ boundNodes[costNodeIdx] ] = 1;
    }
}

void LinkGraph::getFromLongPath(int& costNodeIdx, std::vector<int>& _path)
{
//    std::cout<<"getFromLongPath"<<std::endl;

    int minbw = 2147483640;
    for(size_t i = 1; i < _path.size(); i++)
    {
        if( edgeVec[_path[i]].bw < minbw)
        {
            minbw = edgeVec[_path[i]].bw;
        }
    }

    if(minbw < edgeVec[_path[0]].bw)
    {
        //set bound node as mandatory Nodes, other nodes as impossible node
        mandatoryNodes.push_back( boundNodes[costNodeIdx] );
        mandatoryNodesSelected[ boundNodes[costNodeIdx] ] = 1;

        for(size_t i = 2; i < _path.size(); i++)
        {
            impossibleNodes.push_back( edgeVec[_path[i]].from );
            impossibleNodesSelected[ edgeVec[_path[i]].from ] = 1;
        }
    }
    else
    {
        //set all node on the path as impossible node
        for(size_t i = 1; i < _path.size(); i++)
        {
            impossibleNodes.push_back( edgeVec[_path[i]].from );
            impossibleNodesSelected[ edgeVec[_path[i]].from ] = 1;
        }
    }
}

void LinkGraph::getFromLongPathBetweenCostNode(int& costNodeIdx, std::vector<int>& _path)
{
//    std::cout<<"getFromLongPathBetweenCostNode"<<std::endl;
    if(_path.size() < 3)
    {
        std::cout<<"bug in getFromLongPathBetweenCostNode"<<std::endl;
        return;
    }

    int minbw = 2147483640;
    for(size_t i = 1; i < _path.size() - 1; i++)
    {
        if( edgeVec[_path[i]].bw < minbw)
        {
            minbw = edgeVec[_path[i]].bw;
        }
    }

    if((minbw < edgeVec[_path.front()].bw) && (minbw < edgeVec[_path.back()].bw) )
    {
        //set start and end node on the path as mandatory node , other node as impossible node
        mandatoryNodes.push_back( edgeVec[_path[1]].from );
        mandatoryNodesSelected[ edgeVec[_path[1]].from ] = 1;

        mandatoryNodes.push_back( edgeVec[_path.back()].from );
        mandatoryNodesSelected[ edgeVec[_path.back()].from ] = 1;

        for(size_t i = 2; i < _path.size() - 1; i++)
        {
            impossibleNodes.push_back( edgeVec[_path[i]].from );
            impossibleNodesSelected[ edgeVec[_path[i]].from ] = 1;
        }
    }
    else
    {
        //as the bigger flow node as mandatory node ,other node as impossible node
        int flag = edgeVec[_path.front()].bw > edgeVec[_path.back()].bw ? 1 : _path.size() - 1;

        mandatoryNodes.push_back( edgeVec[_path[flag]].from );
        mandatoryNodesSelected[ edgeVec[_path[flag]].from ] = 1;

        for(size_t i = 1; i < _path.size(); i++)
        {
            if(flag == (int)i) continue;

            impossibleNodes.push_back( edgeVec[_path[i]].from );
            impossibleNodesSelected[ edgeVec[_path[i]].from ] = 1;
        }
    }
}

void LinkGraph::cancelEdgeVisitState(std::vector<int>& _path)
{
    int to = 0,from = 0;
    for(size_t i = 0; i < _path.size(); i++)
    {
        edgeVec[ _path[i] ].isVisit = 0;
        from = edgeVec[ _path[i] ].from;
        to = edgeVec[ _path[i] ].to;

        for(size_t j = 0; j < nodeVec[to].adjacentNodes.size(); j++)
        {
            if(nodeVec[to].adjacentNodes[j] == from)
            {
                edgeVec[ nodeVec[to].adjacentEdges[j] ].isVisit = 0;
                break;
            }
        }
    }
}

int LinkGraph::getValidEdgesForEachNode(int node, std::vector<int>& _validEdges)
{
    for(size_t i = 0; i < nodeVec[node].adjacentEdges.size(); i++)
    {
        if(!edgeVec[ nodeVec[node].adjacentEdges[i] ].isCutted)
        {
            _validEdges.push_back(nodeVec[node].adjacentEdges[i]);
        }
    }

    return (int)_validEdges.size();
}

void LinkGraph::branchCutting()
{
    //get all node which degree = 1
    std::vector<int> candidates;
    for(int i = 0; i < commonNodeNum; i++)
    {
        if(nodeVec[i].adjacentNodes.size() == 1)
        {
            candidates.push_back(i);
        }
    }

    //branch cutting
    std::vector<int> path;
    for(size_t i = 0; i < candidates.size(); i++)
    {
//        std::cout<<"branch start: "<<candidates[i]<<std::endl;
        findUnipath(candidates[i], path, true, false);//set cutting, no cross bound node

        for(size_t j = 0; j < path.size(); j++)
        {
//            std::cout<<edgeVec[ path[j]].from<<" -> ";

            impossibleNodesSelected[ edgeVec[ path[j] ].from ] = 1;
            impossibleNodes.push_back( edgeVec[ path[j] ].from );
        }
//        std::cout<<"("<<edgeVec[path.back()].to<<")"<<std::endl;

        path.clear();
    }
//    std::cout<<std::endl;

//    std::cout<<std::endl<<"cutting "<<impossibleNodes.size()<<" : ";
//    for(size_t i = 0; i < impossibleNodes.size(); i++)
//    {
//        std::cout<<impossibleNodes[i]<<"("<<impossibleNodesSelected[ impossibleNodes[i] ]<<") ";
//    }
//    std::cout<<std::endl;
//
}

void LinkGraph::findUnipath(const int& _pathStart,
                            std::vector<int>& _path,
                            bool setCutting,
                            bool isCrossBound)
{
//    std::cout<<"_pathStart: "<<_pathStart<<std::endl;
    if(!isCrossBound && nodeTypes[_pathStart] == 1)//can not cross bound node
    {
//        std::cout<<"bound"<<std::endl;
        return;
    }

    int usefulEdgeNum = 0;
    int usefulEdge = 0;
    int adjEdge = 0;
    for(size_t i = 0; i < nodeVec[_pathStart].adjacentEdges.size(); i++)
    {
        adjEdge = nodeVec[_pathStart].adjacentEdges[i];
        if( !edgeVec[adjEdge].isCutted && !edgeVec[adjEdge].isVisit )
        {
            usefulEdgeNum++;
            usefulEdge = adjEdge;
        }
    }

    if(usefulEdgeNum != 1)
    {
//        std::cout<<"out"<<std::endl;
        return;
    }

    //branch cutting

    if(setCutting)
    {
        edgeVec[ usefulEdge ].isCutted = 1;
    }
    else
    {
        edgeVec[ usefulEdge ].isVisit = 1;
    }

//        std::cout<< "cutted or visit: " << usefulEdge <<std::endl;
    int to = edgeVec[ usefulEdge ].to;
    for(size_t i = 0; i < nodeVec[to].adjacentNodes.size(); i++)
    {
        if(nodeVec[to].adjacentNodes[i] == _pathStart)
        {
            if(setCutting)
            {
                edgeVec[ nodeVec[to].adjacentEdges[i] ].isCutted = 1;
            }
            else
            {
                edgeVec[ nodeVec[to].adjacentEdges[i] ].isVisit = 1;
            }
//                std::cout<<"cutted or visit: "<<nodeVec[to].adjacentEdges[i]<<std::endl;

            break;
        }
    }

//    for(size_t i = 0; i < commonNodeEdgeNum * 4 + costNodeNum * 2; i++)
//    {
//        if(edgeVec[i].isCutted)
//        {
//            std::cout<<edgeVec[i].idx<<" : "<<edgeVec[i].from<<" -> "<<edgeVec[i].to<<std::endl;
//        }
//    }
//    std::cout<<std::endl;

    //save unipath
    _path.push_back(usefulEdge);

    findUnipath(edgeVec[ usefulEdge ].to, _path, setCutting, isCrossBound);
}


