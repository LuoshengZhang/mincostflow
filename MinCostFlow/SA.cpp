#include "SA.h"
#include "graph.h"
#include <time.h>
#include <math.h>
#include <stdio.h>
#include <algorithm>
#include <iostream>
#define INF 2147483640

void ServerGroup::print()
{
    //std::cout<<name<<" : ";
    std::cout<<"server number:"<<serverVec.size()<<std::endl;
    for(size_t i = 0; i < serverVec.size(); i ++)
    {
        std::cout<<serverVec[i]<<" ";
    }
    std::cout<<"(cost: "<<cost<<")";
    std::cout<<std::endl;
}

SimulatedAnnealing::SimulatedAnnealing(LinkGraph* _graph, MinCostFlow* flow, SAFitness _fitness)
{
    graph = _graph;
    fitness = _fitness;
    minCostCalculator = flow;
    stopped = false;
    mostServerNum = graph->costNodeNum - 1;

/*
//   graph->sortCostNodeAccordingDemand(0.7, 0.3, 0.0);
    std::cout<<"costnode number:"<<graph->costNodePairs.size()<<std::endl;
    for(size_t i = 0; i < graph->costNodePairs.size(); i++)
    {
        bestGroup.serverVec.push_back(graph->costNodePairs[i].first);
        goodServerVec.push_back(graph->costNodePairs[i].first);
//        weightedServerVec.push_back(graph->sortedCommonNodes[graph->commonNodeNum-1-i]);
 //       weightedServerVec.push_back(graph->costNodePairs[i].first);
    }

    std::cout<<"maxdegree:";
    int maxdegree=0;
    for(int i=0;i<graph->costNodeNum;i++)
    {
        if(graph->nodeVec[i].degree>maxdegree)
            maxdegree=graph->nodeVec[i].degree;
    }
    std::cout<<maxdegree<<std::endl;
    int maxdegreenum=0;
    for(int i=0;i<graph->costNodeNum;i++)
    {
        if(graph->nodeVec[i].degree==maxdegree)
            maxdegreenum++;
    }
    std::cout<<"maxdegree node number:"<<maxdegreenum<<std::endl;
*/

    bestGroup.serverVec = graph->boundNodes;
    bestGroup.cost = graph->costNodeNum * graph->eachServerCost;
    topGroup = bestGroup;

    newGroupSelected.assign(graph->commonNodeNum, 0);
    for(int i = 0; i < graph->commonNodeNum; i++)
    {
        if(graph->mandatoryNodesSelected[i])
            continue;
        if(graph->impossibleNodesSelected[i])
            continue;
        candidateNodes.push_back(i);
    }

    if(graph->mandatoryNodes.size() == bestGroup.serverVec.size())
        SetSwitch(true);

    if(!stopped)
    {
        InitServerGroup();
//        GetInitGroup();
        curGroup = bestGroup;
        topGroup = bestGroup;
    }
}

SimulatedAnnealing::~SimulatedAnnealing()
{

}

//获取模拟退火求得的最优服务器数组
void SimulatedAnnealing::GetServerGroup(char* filename)
{
    if(!stopped)
        Simulation();

    if(topGroup.cost < bestGroup.cost)
    {
        graph->reconstructSuperSource(topGroup.serverVec, topGroup.serverVec.size());
//        topGroup.cost = minCostCalculator->getFinalCost() + topGroup.serverVec.size() * graph->eachServerCost;
    }
    else
    {
        graph->reconstructSuperSource(bestGroup.serverVec, bestGroup.serverVec.size());
//        bestGroup.cost = minCostCalculator->getFinalCost() + bestGroup.serverVec.size() * graph->eachServerCost;
    }

    minCostCalculator->getFinalCost();
    minCostCalculator->saveResultToFile(filename);

    //topGroup.print();
    //report();
    //printf("final cost: %d\nbest cost: %d\n", bestGroup.cost, topGroup.cost);

    printf("best cost: %d\ntop cost: %d\nservernum: %d\n", bestGroup.cost, topGroup.cost, (int)topGroup.serverVec.size());

}

//模拟过程入口
void SimulatedAnnealing::Simulation()
{
//printf("simulation");
    int PrL = 0, PrF = 0;
    double temp = fitness.initTemp;

    while(!stopped)
    {
        for(int k = 0; k < fitness.inLoop; ++k)
        {
            UpdateGroup();     // 产生新的服务器组

            double delta = newGroup.cost - curGroup.cost;
            if(delta < 0)  //新解比当前解更优，更新当前解
            {
                curGroup = newGroup;
                if(topGroup.cost > curGroup.cost)
                {
                    topGroup = curGroup;
                    if(mostServerNum > (topGroup.cost / graph->eachServerCost))
                    {
                        mostServerNum = topGroup.cost / graph->eachServerCost;
                    }
                }

                PrL = 0;
                PrF = 0;
            }
            else
            {
                double rd = rand() / (RAND_MAX + 1.0);
                if(newGroup.cost<INF && exp(-delta / temp) > rd)   //如果找到比当前更差的解，以一定概率接受该解，并且这个概率会越来越小
                {
                    curGroup = newGroup;
                }
                PrL++;
            }

            if(PrL > fitness.upperLimit)
            {
                PrF++;
                break;
            }
            if(stopped)
                break;
        }

        if(curGroup.cost < newGroup.cost)
            bestGroup = curGroup;

        if(PrF > fitness.outLoop || temp < fitness.finalTemp)
            break;

        temp *= fitness.deltaTemp;
    }

    //std::cout<<"prl: "<<PrL<<" upperLimit: "<<fitness.upperLimit<<std::endl;
    //std::cout<<"prf: "<<PrF<<" outLoop: "<<fitness.outLoop<<std::endl;
    //std::cout<<"finalTemp: "<<temp<<" initTemp: "<<fitness.initTemp<<std::endl;
////printf("/\n");
}

void SimulatedAnnealing::SetSwitch(bool OnOrOff)
{
    stopped = OnOrOff;
}

//初始化服务器数组（从边界节点中减去任意一个节点的n种组合）
void SimulatedAnnealing::InitServerGroup()
{
////printf("init  ");
    int mincost = 0;
    if(mostServerNum == (int)graph->mandatoryNodes.size())
    {
        graph->reconstructSuperSource(graph->mandatoryNodes, graph->mandatoryNodes.size());

        mincost = minCostCalculator->getCost();
        if(mincost < INF)
            mincost += graph->mandatoryNodes.size() * graph->eachServerCost;
        if(mincost < bestGroup.cost)
        {
            bestGroup.cost = mincost;
            bestGroup.serverVec = graph->mandatoryNodes;
        }
        SetSwitch(true);
        return;
    }

    ServerGroup tempServer2 = topGroup;
    ServerGroup tempServer3 = topGroup;
    ServerGroup tempServer23 = topGroup;
    ServerGroup tempServer4 = topGroup;
    ServerGroup tempServer234 = topGroup;
    ServerGroup tempServer24 = topGroup;
    ServerGroup tempServer34 = topGroup;

//产生初始解集，删掉边界节点度为n的可行解,n=2,3,4
    for(int i = (int)topGroup.serverVec.size()-1; i >= 0 ; i--)
    {
        if(graph->mandatoryNodesSelected[topGroup.serverVec[i]]
           || graph->nodeVec[topGroup.serverVec[i]].degree > 4)   // 在graph->mandatoryNodes中找到该节点了，则该节点不能被删除，继续下一组
        {
            continue;
        }

        if(graph->impossibleNodesSelected[topGroup.serverVec[i]])
        {
            tempServer2.serverVec.erase(tempServer2.serverVec.begin()+i);
            tempServer3.serverVec.erase(tempServer3.serverVec.begin()+i);
            tempServer23.serverVec.erase(tempServer23.serverVec.begin()+i);
            tempServer4.serverVec.erase(tempServer4.serverVec.begin()+i);
            tempServer234.serverVec.erase(tempServer234.serverVec.begin()+i);
            tempServer24.serverVec.erase(tempServer24.serverVec.begin()+i);
            tempServer34.serverVec.erase(tempServer34.serverVec.begin()+i);
            continue;
        }

        if(graph->nodeVec[topGroup.serverVec[i]].degree < 3)
        {
            tempServer2.serverVec.erase(tempServer2.serverVec.begin()+i);
            tempServer23.serverVec.erase(tempServer23.serverVec.begin()+i);
            tempServer234.serverVec.erase(tempServer234.serverVec.begin()+i);
            tempServer24.serverVec.erase(tempServer24.serverVec.begin()+i);
        }
        else if(graph->nodeVec[topGroup.serverVec[i]].degree < 4)
        {
            tempServer3.serverVec.erase(tempServer3.serverVec.begin()+i);
            tempServer23.serverVec.erase(tempServer23.serverVec.begin()+i);
            tempServer234.serverVec.erase(tempServer234.serverVec.begin()+i);
            tempServer34.serverVec.erase(tempServer34.serverVec.begin()+i);
        }
        else if(graph->nodeVec[topGroup.serverVec[i]].degree < 5)
        {
            tempServer4.serverVec.erase(tempServer4.serverVec.begin()+i);
            tempServer234.serverVec.erase(tempServer234.serverVec.begin()+i);
            tempServer24.serverVec.erase(tempServer24.serverVec.begin()+i);
            tempServer34.serverVec.erase(tempServer34.serverVec.begin()+i);
        }
    }

    graph->reconstructSuperSource(tempServer2.serverVec, tempServer2.serverVec.size());
    mincost = minCostCalculator->getCost();
    if(mincost < INF)
    {
        tempServer2.cost = mincost + tempServer2.serverVec.size() * graph->eachServerCost;
        InitServerVecs.push_back(tempServer2);

        if(tempServer2.cost < bestGroup.cost)
        {
            bestGroup = tempServer2;
        }
    }

    graph->reconstructSuperSource(tempServer3.serverVec, tempServer3.serverVec.size());
    mincost = minCostCalculator->getCost();
    if(mincost < INF)
    {
        tempServer3.cost = mincost + tempServer3.serverVec.size() * graph->eachServerCost;
        InitServerVecs.push_back(tempServer3);

        if(tempServer3.cost < bestGroup.cost)
        {
            bestGroup = tempServer3;
        }
    }

    graph->reconstructSuperSource(tempServer23.serverVec, tempServer23.serverVec.size());
    mincost = minCostCalculator->getCost();
    if(mincost < INF)
    {
        tempServer23.cost = mincost + tempServer23.serverVec.size() * graph->eachServerCost;
        InitServerVecs.push_back(tempServer23);

        if(tempServer23.cost < bestGroup.cost)
        {
            bestGroup = tempServer23;
        }
    }

    graph->reconstructSuperSource(tempServer4.serverVec, tempServer4.serverVec.size());
    mincost = minCostCalculator->getCost();
    if(mincost < INF)
    {
        tempServer4.cost = mincost + tempServer4.serverVec.size() * graph->eachServerCost;
        InitServerVecs.push_back(tempServer4);

        if(tempServer4.cost < bestGroup.cost)
        {
            bestGroup = tempServer4;
        }
    }

    graph->reconstructSuperSource(tempServer234.serverVec, tempServer234.serverVec.size());
    mincost = minCostCalculator->getCost();
    if(mincost < INF)
    {
        tempServer234.cost = mincost + tempServer234.serverVec.size() * graph->eachServerCost;
        InitServerVecs.push_back(tempServer234);

        if(tempServer234.cost < bestGroup.cost)
        {
            bestGroup = tempServer234;
        }
    }

    graph->reconstructSuperSource(tempServer24.serverVec, tempServer24.serverVec.size());
    mincost = minCostCalculator->getCost();
    if(mincost < INF)
    {
        tempServer24.cost = mincost + tempServer24.serverVec.size() * graph->eachServerCost;
        InitServerVecs.push_back(tempServer24);

        if(tempServer24.cost < bestGroup.cost)
        {
            bestGroup = tempServer24;
        }
    }

    graph->reconstructSuperSource(tempServer34.serverVec, tempServer34.serverVec.size());
    mincost = minCostCalculator->getCost();
    if(mincost < INF)
    {
        tempServer34.cost = mincost + tempServer34.serverVec.size() * graph->eachServerCost;
        InitServerVecs.push_back(tempServer34);

        if(tempServer34.cost < bestGroup.cost)
        {
            bestGroup = tempServer34;
        }
    }

    printf("InitServerVecs.size:%d \n", (int)InitServerVecs.size());

    if(!InitServerVecs.size())
    {
        InitServerVecs.push_back(topGroup);
        mostServerNum = graph->costNodeNum - 1;
    }
}

void SimulatedAnnealing::GetInitGroup()
{
    std::vector<int> chooseServerVec;

    generateRandVector(chooseServerVec, 0, InitServerVecs.size()-1, 1);  // 计算随机选第几组服务器
    while(InitServerVecs.size() && (int)InitServerVecs[chooseServerVec[0]].serverVec.size() > mostServerNum)
    {
        InitServerVecs.erase(InitServerVecs.begin() + chooseServerVec[0]);
        generateRandVector(chooseServerVec, 0, InitServerVecs.size()-1, 1);
    }

    if(InitServerVecs.size())
    {
        newGroup = InitServerVecs[chooseServerVec[0]];
    }
    else
    {
        newGroup = bestGroup;
    }
//printf("/\n");
}

//更新服务器组（增加/减少/重置）
void SimulatedAnnealing::UpdateGroup()
{
    std::vector<int> chooseServerVec;
    generateRandVector(chooseServerVec, 0, fitness.sum, 1);  // 计算选哪种 Q/addNum=0 增加服务器 Q/addNum=1 减少服务器 Q/addNum=2 初始化服务器组

    switch(chooseServerVec[0]/fitness.addNum)
    {
    case 0:
 //       if(chooseServerVec[0]/fitness.WeightedNum==1)   // Q/WeightedNum=1 从权值大的节点中选择要增加的服务器，否则从所有普通节点中选择
 //           AddWeightedServerGroup();
 //       else
            AddServerGroup();
        break;
    case 1:
        SubServerGroup();
        break;
//    case 2:
//        SwapServerGroup();
//        break;
    default:
//        GetInitGroup();
        break;
    }
}

//增加服务器（从所有普通节点中选择）
void SimulatedAnnealing::AddServerGroup()
{
//printf("add  ");
    if((int)curGroup.serverVec.size() >= mostServerNum) //当前服务器个数最大时，不能再增加了，初始化服务器
    {
        GetInitGroup();
        return;
    }

    std::vector<int> chooseServerVec;

    int addNum = 1; //初级和中级用例时每次增加一个
    if(graph->commonNodeNum > 500)  // 高级用例时每次随机增加1-3个
    {
        generateRandVector(chooseServerVec, 1, 3, 1);  // 计算随机增加几个服务器
        addNum = chooseServerVec[0] < mostServerNum - (int)curGroup.serverVec.size()
        ? chooseServerVec[0] : mostServerNum - (int)curGroup.serverVec.size();
    }

    //add candidates
    tmpCandidateNodes = candidateNodes;
    for(size_t i = 0; i < curGroup.serverVec.size(); i++)
    {
        vecIter = std::find(tmpCandidateNodes.begin(), tmpCandidateNodes.end(), curGroup.serverVec[i]);
        if(vecIter != tmpCandidateNodes.end())
        {
            tmpCandidateNodes.erase(vecIter);
        }
    }

    addNum = (addNum < (int)tmpCandidateNodes.size()) ? addNum : tmpCandidateNodes.size();
    if(!addNum)
    {
        GetInitGroup();
        return;
    }

    int mincost = 0;
    size_t maxLoop = tmpCandidateNodes.size() * 2;
    for(size_t i = 0; i < maxLoop; i++)
    {
        newGroup = curGroup;
        newGroup.cost = INF;

        addCandidates = tmpCandidateNodes;
//        updateVecSelected(newGroup.serverVec, newGroupSelected);
        generateRandVector(chooseServerVec, 0, (int)addCandidates.size() - 1, addNum);

        for(int j = 0; j < addNum; j++)// 随机选择普通节点的几个点
        {
            newGroup.serverVec.push_back( addCandidates[ chooseServerVec[j] ] );

//            generateRandVector(chooseServerVec, 0, (int)candidateNodes.size()-1, 1);
//            while(newGroupSelected[candidateNodes[chooseServerVec[0]]])// 在当前服务器组有新增节点编号时，再次取随机数
//            {
//                generateRandVector(chooseServerVec, 0, (int)candidateNodes.size()-1, 1);
//            }
//            newGroup.serverVec.push_back(candidateNodes[chooseServerVec[0]]);//将该节点放入服务器组
//            newGroupSelected[candidateNodes[chooseServerVec[0]]] = 1;
        }

        graph->reconstructSuperSource(newGroup.serverVec, newGroup.serverVec.size());
        mincost = minCostCalculator->getCost();

        if(mincost < INF)
        {
            mincost += newGroup.serverVec.size() * graph->eachServerCost;
            newGroup.cost = mincost;

            if(!fitness.isTaboo || !tabooSearch(fitness.tabooNum, newGroup))
            {
                InitServerVecs.push_back(newGroup);
                break;
            }
        }
    }

    if(newGroup.cost >= INF)
        GetInitGroup();

//printf("/\n");
}

//减少服务器
void SimulatedAnnealing::SubServerGroup()
{
 //printf("sub  ");

    if(curGroup.serverVec.size() - graph->mandatoryNodes.size()<1) //当前服务器个数最小时，不能再减少了，初始化服务器
    {
        GetInitGroup();
        return;
    }

    std::vector<int> chooseServerVec;
    int subNum = 1;
    if(graph->commonNodeNum > 500)  // 高级用例时每次随机增加1-3个
    {
        generateRandVector(chooseServerVec, 1, 5, 1);  // 计算随机增加几个服务器
        subNum = chooseServerVec[0] < (int)(curGroup.serverVec.size() - graph->mandatoryNodes.size())
        ? chooseServerVec[0] : (int)(curGroup.serverVec.size() - graph->mandatoryNodes.size());
    }

    //sub candidates
    tmpCandidateNodes = curGroup.serverVec;
    for(size_t i = 0; i < tmpCandidateNodes.size(); i++)
    {
        if(graph->mandatoryNodesSelected[tmpCandidateNodes[i]])
        {
            tmpCandidateNodes.erase(tmpCandidateNodes.begin() + i);
            i--;
        }
    }

    subNum = (subNum < (int)tmpCandidateNodes.size()) ? subNum : tmpCandidateNodes.size();
    if(!subNum)
    {
        GetInitGroup();
        return;
    }

    int mincost = 0;
    size_t maxLoop = tmpCandidateNodes.size() * 2;
    for(size_t i = 0; i < maxLoop; i++)
    {
        newGroup = curGroup;
        newGroup.cost = INF;

        subCandidates = tmpCandidateNodes;
        generateRandVector(chooseServerVec, 0, subCandidates.size() - 1, subNum);

        for(int j = 0; j < subNum; j++)// 随机选择删除的几个点
        {
            vecIter = std::find(newGroup.serverVec.begin(), newGroup.serverVec.end(), subCandidates[ chooseServerVec[j] ]);
            newGroup.serverVec.erase(vecIter);
//            generateRandVector(chooseServerVec, 0, newGroup.serverVec.size()-1, 1);
//            while(graph->mandatoryNodesSelected[newGroup.serverVec[chooseServerVec[0]]]) //当前节点为必选节点
//            {
//                generateRandVector(chooseServerVec, 0, newGroup.serverVec.size()-1, 1);
//            }
//            newGroup.serverVec.erase(newGroup.serverVec.begin() + chooseServerVec[0]);
        }

        graph->reconstructSuperSource(newGroup.serverVec, newGroup.serverVec.size());
        mincost = minCostCalculator->getCost();

        if(mincost < INF)
        {
            mincost += newGroup.serverVec.size() * graph->eachServerCost;
            newGroup.cost = mincost;

            if(!fitness.isTaboo || !tabooSearch(fitness.tabooNum, newGroup))
            {
                InitServerVecs.push_back(newGroup);
                break;
            }
        }
    }

    if(newGroup.cost >= INF)
        GetInitGroup();
//printf("  /\n");
}

/*
void SimulatedAnnealing::replaceByAdjacentNode()
{
    if((int)curGroup.serverVec.size() == mostServerNum) //当前服务器个数最大时，不能再增加了，初始化服务器
    {
        GetInitGroup();
        return;
    }

    newGroup = curGroup;
    if(curGroup.serverVec.size() == graph->mandatoryNodes.size())//no enough node replaced
    {
        return;
    }

    int candidateNode = 0;
    int adjacent = 0;
    bool flag = false;
    int mincost = 0;

//    newGroup.print("newGroup before: ");

    //whether some node is selected of node in newGroup
    updateVecSelected(newGroup.serverVec, newGroupSelected);

    for(size_t i = graph->mandatoryNodes.size(); i < newGroup.serverVec.size(); i++)
    {
        newGroup.serverVec[i] = curGroup.serverVec[i];
        candidateNode = newGroup.serverVec[i];
        newGroupSelected[candidateNode] = 0;

        for(size_t j = 1; j < graph->nodeVec[candidateNode].adjacents.size(); j++)
        {
            adjacent = graph->nodeVec[candidateNode].adjacents[j];
            if(newGroupSelected[adjacent])
            {
                continue;
            }

            newGroup.serverVec[i] = adjacent;

            graph->reconstructSuperSource(newGroup.serverVec, newGroup.serverVec.size());
            mincost = minCostCalculator->getCost();
            if(mincost < INF)
            {
                mincost += newGroup.serverVec.size() * graph->eachServerCost;
                newGroup.cost = mincost;

                if(!tabooSearch(fitness.tabooNum, newGroup))
                {
                    InitServerVecs.push_back(newGroup);
                    newGroupSelected[adjacent] = 1;
                    flag = true;
                    break;
                }
            }
        }

        if(flag)
        {
            break;
        }

        newGroupSelected[candidateNode] = 1;
    }

    if(!flag)
    {
        newGroup = curGroup;
    }

 //   newGroup.print("newGroup replace: ");

<<<<<<< HEAD
}*/


bool SimulatedAnnealing::tabooSearch(size_t layer, ServerGroup& curNewGroup)
{
    bool flag = false;
    size_t len = InitServerVecs.size() < layer ? InitServerVecs.size() : layer;
    for(size_t i = InitServerVecs.size() - 1; i > InitServerVecs.size() - 1 - len; i--)
    {
        if(InitServerVecs[i].cost == curNewGroup.cost)
        {
            flag = true;
            break;
        }
    }
    return flag;
}

void SimulatedAnnealing::updateVecSelected(std::vector<int>& _vec, std::vector<int>& _selected)
{
    for(int i = 0; i < graph->commonNodeNum; i++)
    {
        _selected[i] = 0;
    }

    for(size_t j = 0; j < _vec.size(); j++)
    {
        _selected[_vec[j]] = 1;
    }
}

//生成随机数
bool SimulatedAnnealing::generateRandVector(std::vector<int>& randVector, int lower, int upper, int randNum)
{
    if(lower > upper)
    {
        return false;
    }
    //[lower, upper] randNum
    int len = upper - lower + 1;

    if(len < randNum)
    {
        return false;
    }

    bool flag = (randNum <= len / 2);
    int realRandNum = flag ? randNum : len - randNum;

    std::vector<int> selectedVec;
    selectedVec.resize(len);
    int randIndex = 0;

    while( realRandNum )
    {
        randIndex = rand() % len;

        if(selectedVec[randIndex] == 0)
        {
            selectedVec[randIndex] = 1;
            realRandNum --;
        }
    }

    randVector.clear();
    if(flag)
    {
        for(size_t i = 0; i < selectedVec.size(); i++)
        {
            if(selectedVec[i] == 1)
            {
                randVector.push_back(i + lower);
            }
        }
    }
    else
    {
        for(size_t i = 0; i < selectedVec.size(); i++)
        {
            if(selectedVec[i] == 0)
            {
                randVector.push_back(i + lower);
            }
        }
    }

    return true;
}

void SimulatedAnnealing::report()
{
    std::cout<<"server number:"<<topGroup.serverVec.size()<<std::endl;
    for(size_t i = 0; i < topGroup.serverVec.size(); i ++)
    {
        std::cout<<topGroup.serverVec[i]<<":";
        std::cout<<graph->nodeVec[topGroup.serverVec[i]].degree<<"  ";
    }
    std::cout<<std::endl;


}
