#include "GA.h"
#include <cstdlib>
#include<math.h>
#include<iostream>
#include<ctime>
#include <set>
#include<iomanip>

GeneAlg::GeneAlg(LinkGraph* _graph, MinCostFlow* flow,int popsize,int maxgene,double psrate,double pmrate,char *filename)
{
    graph = _graph;
    minCostCalculator = flow;
    POPSIZE=popsize;
    MAXGENES=maxgene;
    PSRATE=psrate;
    PMRATE=pmrate;
    savefile=filename;

    eachCost=graph->eachServerCost;
    geneLength=graph->commonNodeNum;
    costNodeNum=graph->costNodeNum;
    generation=0;
    stopped=false;

    /*获取与消费节点相连的网络节点编号*/
    for(unsigned int ti = 0; ti < graph->costNodePairs.size(); ti++ )
    {
        costCommon.push_back(graph->costNodePairs[ti].first);
    }
    MAXCOST=costCommon.size()*eachCost;

    starGA();
}

GeneAlg::~GeneAlg()
{
    costCommon.clear();
    population.clear();
    newPopulation.clear();
}

void GeneAlg::starGA()
{
    /*计算网络节点的累加权重*/
    graph->sortCostNodeAccordingDemand(0.7,0.3,0.0);
    for(size_t j=0; j<graph->sortedCommonNodes.size(); j++)
    {
        int temp;
        temp=graph->sortedCommonNodes[j];
        csweight+=graph->nodeVec[temp].weight;
        cweight.push_back(csweight);
    }
    //初始化bestChromosome:为每个消费节点部署一个服务器，后面用于存储最佳个体
    for(int j=0; j<geneLength; j++)
    {
        bestChromosome.gene.push_back(0);
    }
    bestChromosome.serverNumRand=graph->costNodeNum;
    for(int j=0; j<bestChromosome.serverNumRand; j++)
    {
        bestChromosome.serverLocateId.push_back(costCommon[j]);
        bestChromosome.gene[bestChromosome.serverLocateId[j]]=1;
    }
    bestChromosome.minFlowCost=0;
    bestChromosome.buildTotalCost=MAXCOST;
    bestChromosome.fitness=0;

    int generation=0;
    Initialize();
    while(generation<MAXGENES)
    {
        if(!stopped)
        {
            generation++;
            Select();
            Crossover();
            Mutate();
        }
        else
            writeFile(savefile);
    }
    writeFile(savefile);
}

void GeneAlg::Initialize()
{
    int i,j;
//    std::cout<<std::endl;
//    std::cout<<"initialize begin...";
    //std::cout<<std::endl;
    srand((unsigned)time(NULL));

    //生成初始种群
    population.clear();
    for(i=0; i<POPSIZE; i++)
    {
        struct GeneType chromosome;
        for(j=0; j<geneLength; j++)
            chromosome.gene.push_back(0);

        //第一种选取方案：消费节点链接的网络节点当中选择节点作为初始种群
        costCommonAsInitial(chromosome.serverLocateId,chromosome.serverNumRand);

        //第二种选取方案：完全随机
        //chromosome.serverNumRand=1+rand()%(costNodeNum);
        //generateRandVector(chromosome.serverLocateId,0,geneLength,chromosome.serverNumRand);

        //第三种选取方案：按节点权重进行选择
        //CommonWeightAsInitial(chromosome.serverLocateId,chromosome.serverNumRand);

        //第四种选取方案：以模拟退火的解作为初始种群
        //std::vector<std::vector<int> > SAResult;//调用测试
        //std::vector<int> SAResultEach;//调用测试
        //int SAServerNum;//调用测试
        //costCommonAsInitial(SAResultEach,SAServerNum);//调用测试
        //SAResult.push_back(SAResultEach);//调用测试
        //SAGoodIndividualsAsInitial(SAResult,i,chromosome.serverLocateId,chromosome.serverNumRand);

        for(j=0; j<chromosome.serverNumRand; j++)
            chromosome.gene[chromosome.serverLocateId[j]]=1;
        Evaluate(chromosome.serverLocateId,chromosome.serverNumRand,chromosome.minFlowCost,chromosome.buildTotalCost,chromosome.fitness);
        if(chromosome.fitness>=0)
            population.push_back(chromosome);
    }
    if(population.size()==0)
        Initialize();
    else
        KeepBest();
}
//与消费节点相连的网络节点为初始解
void GeneAlg::costCommonAsInitial(std::vector<int>& serverID,int& serverNum)
{
    std::vector<int> costCommonId;
    serverNum=1+rand()%(costNodeNum);
    generateRandVector(costCommonId,0,costCommon.size()-1,serverNum);
    for(int j=0; j<serverNum; j++)
    {
        serverID.push_back(costCommon[costCommonId[j]]);
    }
}
//模拟退火的解作为初始解
void GeneAlg::SAGoodIndividualsAsInitial(std::vector<std::vector<int> >& goodIndividuals,int& popId,std::vector<int> serverid,int servernum)
{
    if(popId<(int)goodIndividuals.size())
    {
        servernum=goodIndividuals[popId].size();
        for(int j=0; j<servernum; j++)
            serverid.push_back(goodIndividuals[popId][j]);
    }
    else
        costCommonAsInitial(serverid,servernum);
}

//根据节点权重选择作为初始解
void GeneAlg::CommonWeightAsInitial(std::vector<int>& serverID,int& serverNum)
{
    double p;
    serverNum=1+rand()%(costNodeNum);
    for(int j=0; j<serverNum; j++)
    {
        p=rand()%((int)csweight)+rand()%1000/1000.0;
        if(p<graph->nodeVec[graph->sortedCommonNodes[0]].weight)
            serverID.push_back(graph->sortedCommonNodes[0]);
        else
        {
            for(size_t k=0; k<graph->sortedCommonNodes.size(); k++)
            {
                if(p>=cweight[graph->sortedCommonNodes.size()-2] && p<cweight[graph->sortedCommonNodes.size()-1])
                    serverID.push_back(graph->sortedCommonNodes[graph->sortedCommonNodes.size()-1]);
                else if(p>=cweight[k] && p<cweight[k+1])
                    serverID.push_back(graph->sortedCommonNodes[k+1]);
            }
        }
    }
}

bool GeneAlg::generateRandVector(std::vector<int>& randVector, int lower, int upper, int randNum)
{
    randVector.clear();
    if(lower > upper)
    {
        return false;
    }
    int len = upper - lower + 1;

    if(len < randNum)
    {
        return false;
    }

    bool flag = (randNum <= len / 2);
    int realRandNum = flag ? randNum : len - randNum;

    std::vector<int> selectedVec;
    selectedVec.resize(len);
    int randIndex = 0;

    while( realRandNum )
    {
        randIndex = rand() % len;

        if(selectedVec[randIndex] == 0)
        {
            selectedVec[randIndex] = 1;
            realRandNum --;
        }
    }

    if(flag)
    {
        for(size_t i = 0; i < selectedVec.size(); i++)
        {
            if(selectedVec[i] == 1)
            {
                randVector.push_back(i + lower);
            }
        }
    }
    else
    {
        for(size_t i = 0; i < selectedVec.size(); i++)
        {
            if(selectedVec[i] == 0)
            {
                randVector.push_back(i + lower);
            }
        }
    }

    return true;
}

void GeneAlg::Evaluate(std::vector<int> serverid,int servernum,int &mincost,int &buildcost,int &fitness)
{
    graph->reconstructSuperSource(serverid, servernum);
    mincost=minCostCalculator->getCost();
    if(mincost>MAXCOST )
        fitness=-1;
    else
    {
        buildcost=eachCost*servernum;
        if(buildcost+mincost>MAXCOST)
            fitness=-1;
        else
            fitness=MAXCOST-mincost-buildcost;
    }
}

void GeneAlg::KeepBest()
{
//    std::cout<<std::endl;
//    std::cout<<"KeepBest begin:";
    //std::cout<<std::endl;

    /*寻找当前种群中的最佳个体，与bestChromosome个体比较*/
    if(population.size()!=0)
    {
        int bestfitness=bestChromosome.fitness;
        curBest=0;
        for(size_t it=0; it<population.size(); ++it)
        {
            if(population[it].fitness>bestfitness)
            {
                curBest=it;
                bestfitness=population[it].fitness;
            }
        }
        if(bestfitness>bestChromosome.fitness)
            bestChromosome=population[curBest];//将当前最佳个体存储到bestChromosome
    }

    std::cout<<std::endl;
    std::cout<<"curbest cost :";
    std::cout<<bestChromosome.buildTotalCost+bestChromosome.minFlowCost;
}

/*准备配对池，保证最优秀的个体得以生存*/
void GeneAlg::Select()
{
    int i,eliteNum=0;
    newPopulation.clear();
    for(size_t j=0; j<population.size(); j++)  //拷贝上一代的精英个体到当前种群
    {
        //if(population[j].fitness>0)
        if(population[j].fitness>bestChromosome.fitness*0.5)
        {
            newPopulation.push_back(population[j]);
            eliteNum++;
        }
    }
    for(i=eliteNum; i<POPSIZE; i++)
    {
        struct GeneType chromosome;
        for(int j=0; j<geneLength; j++)
        {
            chromosome.gene.push_back(0);
        }
        //if(i<(POPSIZE-eliteNum)/5)
        //CommonWeightAsInitial(chromosome.serverLocateId,chromosome.serverNumRand);//根据网络节点的权重选择
        //else
        costCommonAsInitial(chromosome.serverLocateId,chromosome.serverNumRand);//消费节点相连的网络节点

        for(int j=0; j<chromosome.serverNumRand; j++)
            chromosome.gene[chromosome.serverLocateId[j]]=1;
        Evaluate(chromosome.serverLocateId,chromosome.serverNumRand,chromosome.minFlowCost,chromosome.buildTotalCost,chromosome.fitness);
        if(chromosome.fitness>=0)
            newPopulation.push_back(chromosome);
    }
    if(newPopulation.size()==0)
        Select();
    else
    {
        population.swap(newPopulation);//新种群拷贝回当前种群
        KeepBest();//找出当前最优
    }
}

//选择两个个体来杂交，单点杂交
void GeneAlg::Crossover()
{
    int one = 0;
    int first=0;//count of the number of members chosen
    float x;
    for(size_t i=0; i<population.size(); i++)
    {
        x=rand()%1000/1000.0;
        if(x<PSRATE)
        {
            ++first;
            if(first%2==0) //偶数编号的个体
                Xover(one,i);
            else
                one=i;
        }
    }
}

void GeneAlg::Xover(int one,int two)
{
    int i;
    int point;
    int temp;

    if(geneLength>1)
    {
        if(geneLength==2)
            point=1;
        else
            point=(rand()%(geneLength-1))+1;
        for(i=0; i<point; i++)
        {
            temp=population[one].gene[i];
            population[one].gene[i]=population[two].gene[i];
            population[two].gene[i]=temp;
        }
    }
}

void GeneAlg::Mutate()
{
    int j;
    float x;
    std::vector<struct GeneType>::iterator i;
    for(i=population.begin(); i!=population.end();)
    {
        for(j=0; j<geneLength; j++)
        {
            x=rand()%1000/1000.0;
            if(x<PMRATE)
                i->gene[j]=!i->gene[j];
        }
        Evaluate(i->serverLocateId,i->serverNumRand,i->minFlowCost,i->buildTotalCost,i->fitness);
        if(i->fitness>MAXCOST||i->fitness<0)
            population.erase(i);
        else
            ++i;
    }
    KeepBest();//找出当前最优
}

void GeneAlg::writeFile(char* filename)
{
//    std::cout<<std::endl;
//    std::cout<<"write begin ...";

    graph->reconstructSuperSource(bestChromosome.serverLocateId, bestChromosome.serverNumRand);
    minCostCalculator->getFinalCost();
    minCostCalculator->saveResultToFile(filename);
    bestCost=bestChromosome.buildTotalCost+bestChromosome.minFlowCost;

    //输出测试
//    std::cout<<std::endl;
//    std::cout<<std::endl;
    std::cout<<"     totalcost      buildCost        minCost"<<std::endl;
    std::cout<<std::setw(14)<<bestCost<<" ";
    std::cout<<std::setw(14)<<bestChromosome.buildTotalCost<<" ";
    std::cout<<std::setw(14)<<bestChromosome.minFlowCost<<" ";
    std::cout<<std::endl;
}

void GeneAlg::SetSwitch(bool OnOrOff)
{
    stopped = OnOrOff;
}
